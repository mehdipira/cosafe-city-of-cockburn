
# coding: utf-8

# In[13]:


# Connect to the GIS
from arcgis.gis import GIS
from arcgis import features
import pandas as pd
# import config_cfs_assign
# from getpass import getpass
from arcgis import geometry
from copy import deepcopy
import datetime
import smtplib
#from datetime import datetime
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import email.encoders as encoders

# url = 'https??ab-prd-gis-03.cockburn.wa.gov.au?portal'
# url = url.replace('??', '://')
# url = url.replace('?', ':/')
url = 'https://ab-prd-gis-03.cockburn.wa.gov.au/portal'
username = 'adm.esriservices'
password = 'D$Usl\gr'
gis = GIS(url, username, password)

cfs_result,cfs_rows,assignment_result,assignment_rows,worker_result,worker_rows = None, None, None, None, None, None
cfs_layers,cfs_flayer,cfs_fset = None, None, None
assignment_layers,assignment_flayer,assignment_fset = None, None, None
worker_layers,worker_flayer,worker_fset = None, None, None
new_feature = None
worker_id = None
av_result,anc_result,asb_result,cfa_result,cli_result,hw_result,nc_result,sp_result,sb_result,ta_result,th_result,tres_result,van_result,vpc_result = None,None,None,None,None,None,None,None,None,None,None,None,None,None
av_layers,anc_layers,asb_layers,cfa_layers,cli_layers,hw_layers,nc_layers,sp_layers,sb_layers,ta_layers,th_layers,tres_layers,van_layers,vpc_layers = None,None,None,None,None,None,None,None,None,None,None,None,None,None
av_flayer,anc_flayer,asb_flayer,cfa_flayer,cli_flayer,hw_flayer,nc_flayer,sp_flayer,sb_flayer,ta_flayer,th_flayer,tres_flayer,van_flayer,vpc_flayer = None,None,None,None,None,None,None,None,None,None,None,None,None,None
av_fset,  anc_fset,  asb_fset,  cfa_fset,  cli_fset,  hw_fset,  nc_fset,  sp_fset,  sb_fset,  ta_fset,  th_fset,  tres_fset,  van_fset,  vpc_fset   = None,None,None,None,None,None,None,None,None,None,None,None,None,None
av_rows,  anc_rows,  asb_rows,  cfa_rows,  cli_rows,  hw_rows,  nc_rows,  sp_rows,  sb_rows,  ta_rows,  th_rows,  tres_rows,  van_rows,  vpc_rows   = None,None,None,None,None,None,None,None,None,None,None,None,None,None

# To reset some fields in custom feature service
def flush_up(temp):
    temp.attributes['description'] = None
    temp.attributes['status'] = None
    temp.attributes['notes'] = None
#     temp.attributes['priority'] = -1
    temp.attributes['assignmenttype'] = -1
    temp.attributes['workorderid'] = -1
    temp.attributes['duedate'] = None

    return temp
def getFeatureServices():
    #search for the custom feature service
    global cfs_result
    global cfs_layers
    global cfs_flayer
    global cfs_fset
    global cfs_rows
    cfs_result = gis.content.search('type: "Feature Service" AND title:"Dispatch_CoSafe_CFS"')
    #access the item's feature layers
    cfs_layers = cfs_result[0].layers
    cfs_flayer = cfs_layers[0]
    #query all the features and display table
    cfs_fset = cfs_layers[0].query()
    if len(cfs_fset.features) > 0:
        cfs_rows = cfs_fset.sdf
    else:
        pass

    #search for the assignments feature service
    global assignment_result
    global assignment_layers
    global assignment_flayer
    global assignment_fset
    global assignment_rows
    assignment_result = gis.content.search('type: "Feature Service" AND title:"assignments_836f26b1ba584eeca72b517ac71715a7"')
    #access the item's feature layers
    assignment_layers = assignment_result[0].layers
    assignment_flayer = assignment_layers[0]
    #query all the features and display its table
    assignment_fset = assignment_layers[0].query()
    if len(assignment_fset.features) > 0:
        assignment_rows = assignment_fset.sdf
    else:
        pass

    #search for the Workers feature service
    global worker_result
    global worker_layers
    global worker_flayer
    global worker_fset
    global worker_rows
    worker_result = gis.content.search('type: "Feature Service" AND title:"workers_836f26b1ba584eeca72b517ac71715a7"')
    #access the item's feature layers
    worker_layers = worker_result [0].layers
    worker_flayer = worker_layers[0]
    #query all the features and display table
    worker_fset = worker_layers[0].query()
    if len(worker_fset.features) > 0:
         worker_rows = worker_fset.sdf
    else:
        pass
   
    
    #search for the survey feature services
    global av_result
    global av_layers
    global av_flayer
    global av_fset
    global av_rows
    av_result = gis.content.search('type: "Feature Service" AND title:"Abandoned_Vehicle"')
    av_layers = av_result[0].layers
    av_flayer = av_layers[0]
    av_fset   = av_layers[0].query()
    if len(av_fset.features) > 0:
        av_rows = av_fset.sdf
    else:
        pass

#     #search for the survey feature services
#     global anc_result
#     global anc_layers
#     global anc_flayer
#     global anc_fset
#     global anc_rows
#     anc_result = gis.content.search('type: "Feature Service" AND title:"Alarm Non-City"')
#     anc_layers = anc_result[0].layers
#     anc_flayer = anc_layers[0]
#     anc_fset   = anc_layers[0].query()
#     if len(anc_fset.features) > 0:
#         anc_rows = anc_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global asb_result
#     global asb_layers
#     global asb_flayer
#     global asb_fset
#     global asb_rows
#     asb_result = gis.content.search('type: "Feature Service" AND title:"Anti Social Behaviour"')
#     asb_layers = asb_result[0].layers
#     asb_flayer = asb_layers[0]
#     asb_fset   = asb_layers[0].query()
#     if len(asb_fset.features) > 0:
#         asb_rows = asb_fset.sdf
#     else:
#         pass
        
#     #search for the survey feature services
#     global cfa_result
#     global cfa_layers
#     global cfa_flayer
#     global cfa_fset
#     global cfa_rows
#     cfa_result = gis.content.search('type: "Feature Service" AND title:"City_Facility_Alarm"')
#     cfa_layers = cfa_result[0].layers
#     cfa_flayer = cfa_layers[0]
#     cfa_fset   = cfa_layers[0].query()
#     if len(cfa_fset.features) > 0:
#         cfa_rows = cfa_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global cli_result
#     global cli_layers
#     global cli_flayer
#     global cli_fset
#     global cli_rows
#     cli_result = gis.content.search('type: "Feature Service" AND title:"Customer_Let_In"')
#     cli_layers = cli_result[0].layers
#     cli_flayer = cli_layers[0]
#     cli_fset   = cli_layers[0].query()
#     if len(cli_fset.features) > 0:
#         cli_rows = cli_fset.sdf
#     else:
#         pass
    
    #search for the survey feature services
    global hw_result
    global hw_layers
    global hw_flayer
    global hw_fset
    global hw_rows
    hw_result = gis.content.search('type: "Feature Service" AND title:"Holiday_Watch"')
    hw_layers = hw_result[0].layers
    hw_flayer = hw_layers[0]
    hw_fset   = hw_layers[0].query()
    if len(hw_fset.features) > 0:
        hw_rows = hw_fset.sdf
    else:
        pass
    
#     #search for the survey feature services
#     global nc_result
#     global nc_layers
#     global nc_flayer
#     global nc_fset
#     global nc_rows
#     nc_result = gis.content.search('type: "Feature Service" AND title:"Noise_Complaint"')
#     nc_layers = nc_result[0].layers
#     nc_flayer = nc_layers[0]
#     nc_fset   = nc_layers[0].query()
#     if len(nc_fset.features) > 0:
#         nc_rows = nc_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global sp_result
#     global sp_layers
#     global sp_flayer
#     global sp_fset
#     global sp_rows
#     sp_result = gis.content.search('type: "Feature Service" AND title:"Security_Patrol"')
#     sp_layers = sp_result[0].layers
#     sp_flayer = sp_layers[0]
#     sp_fset   = sp_layers[0].query()
#     if len(sp_fset.features) > 0:
#         sp_rows = sp_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global sb_result
#     global sb_layers
#     global sb_flayer
#     global sb_fset
#     global sb_rows
#     sb_result = gis.content.search('type: "Feature Service" AND title:"Suspicious_Behaviour"')
#     sb_layers = sb_result[0].layers
#     sb_flayer = sb_layers[0]
#     sb_fset   = sb_layers[0].query()
#     if len(sb_fset.features) > 0:
#         sb_rows = sb_fset.sdf
#     else:
#         pass
        
#     #search for the survey feature services
#     global ta_result
#     global ta_layers
#     global ta_flayer
#     global ta_fset
#     global ta_rows
#     ta_result = gis.content.search('type: "Feature Service" AND title:"Target_Alerts"')
#     ta_layers = ta_result[0].layers
#     ta_flayer = ta_layers[0]
#     ta_fset   = ta_layers[0].query()
#     if len(ta_fset.features) > 0:
#         ta_rows = ta_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global th_result
#     global th_layers
#     global th_flayer
#     global th_fset
#     global th_rows
#     th_result = gis.content.search('type: "Feature Service" AND title:"Traffic_Hazard"')
#     th_layers = th_result[0].layers
#     th_flayer = th_layers[0]
#     th_fset   = th_layers[0].query()
#     if len(th_fset.features) > 0:
#         th_rows = th_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global tres_result
#     global tres_layers
#     global tres_flayer
#     global tres_fset
#     global tres_rows
#     tres_result = gis.content.search('type: "Feature Service" AND title:"Trespass_Survey"')
#     tres_layers = tres_result[0].layers
#     tres_flayer = tres_layers[0]
#     tres_fset   = tres_layers[0].query()
#     if len(tres_fset.features) > 0:
#         tres_rows = tres_fset.sdf
#     else:
#         pass
    
#     #search for the survey feature services
#     global van_result
#     global van_layers
#     global van_flayer
#     global van_fset
#     global van_rows
#     van_result = gis.content.search('type: "Feature Service" AND title:"Vandalism_Survey"')
#     van_layers = van_result[0].layers
#     van_flayer = van_layers[0]
#     van_fset   = van_layers[0].query()
#     if len(van_fset.features) > 0:
#         van_rows = van_fset.sdf
#     else:
#         pass
    
    #search for the survey feature services
    global vpc_result
    global vpc_layers
    global vpc_flayer
    global vpc_fset
    global vpc_rows
    vpc_result = gis.content.search('type: "Feature Service" AND title:"Vehicle_Prestart_Check"')
    vpc_layers = vpc_result[0].layers
    vpc_flayer = vpc_layers[0]
    vpc_fset   = vpc_layers[0].query()
    if len(vpc_fset.features) > 0:
        vpc_rows = vpc_fset.sdf
    else:
        pass

# To send emails when survey is submitted
def sendEmails(surveyFS, customer_fName, customer_lName, inspect_date, officer):
    # Create an email with the report as an attachment
    emailRecipients = ['akawamura@cockburn.wa.gov.au','dwong@cockburn.wa.gov.au']

    msg = MIMEMultipart()
    msg['From'] = 'gisservices@cockburn.wa.gov.au'
    # recipients = emailRecipients
    msg['To'] =  ", ".join(emailRecipients)
    msg['Subject'] = f'{surveyFS} survey submitted'

    part = MIMEBase('application', "octet-stream")

    # Create the plain-text and HTML version of your message
    text = f"""    Hi,
    This is an automated email from City of Cockburn in regards to {surveyFS}.
    {surveyFS} survey form for {customer_fName} {customer_lName} has been submitted on {inspect_date} by {officer}!"""

    part = MIMEText(text, "plain")
    msg.attach(part)
    print(msg)

    s = smtplib.SMTP('internal.cockburn.wa.gov.au', 25)
    s.ehlo()
    s.starttls()
    s.ehlo()
    password = 'Giscockburn1'
    s.login('gisservices', password)
    print(s)
    s.send_message(msg)
    s.quit()

# webhooks processing to flag custom feature service, workforce assignment and surveys when a survey is submitted
def webhooksProcessing(sRows, sResult, sFset, sFlayer):
    try:
        sKeys = []
        sValues = []
        sDict = {}
        sKeys = [av_oid for av_oid in sRows.objectid]
        sValues = [int(av_sid) for av_sid in sRows.customfeatureid]
        sDict = dict(zip(sKeys, sValues))

        for index, row_cfs in cfs_rows.iterrows():
            if row_cfs.OBJECTID in sDict.values():
                if row_cfs.CUSTOMER_REQUEST_STATUS == 'True':
                    template_cfsfeature = deepcopy(cfs_fset.features[index])
                    template_cfsfeature.attributes['PROCESSED_STATUS'] = 'Complete'
                    cfs_flayer.edit_features(updates = [template_cfsfeature]) 
                    print("CFS Processed Status is complete!")
                for index, av_row in sRows.iterrows():
                    today = datetime.datetime.today()
                    l_date = datetime.datetime.strptime(str(row_cfs['END_DATE']), '%Y-%m-%d %H:%M:%S')
                    delta = l_date - today
                    
                    if av_row['process_status'] == 'FALSE' and row_cfs.CUSTOMER_FIRSTNAME == av_row.custdetails_firstname and                     row_cfs.CUSTOMER_LASTNAME == av_row.custdetails_lastname:

                        if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                            print("within timeframe, sending an email!")
                            if sResult[0].title != 'Vehicle_Prestart_Check':
                                sendEmails(sResult[0].title, av_row.custdetails_firstname, av_row.custdetails_lastname, av_row.created_date, av_row.created_user)
                                template_avfeature = deepcopy(sFset.features[index])
                                template_avfeature.attributes['process_status'] = 'TRUE'
                                sFlayer.edit_features(updates = [template_avfeature])
                                print(f"{sResult[0].title} Processed Status is set to True!")
                                for index, row_ass in assignment_rows.iterrows():
                                    if int(row_ass.workorderid) in sDict.values() and                                         row_ass.assignmenttype == row_cfs.RESPONSE_SURVEY_TYPE and row_ass.status == 1:

                                        #print(row_ass.workorderid, row_cfs.customer_firstname, row_cfs.customer_lastname)
                                        template_assfeature = deepcopy(assignment_fset.features[index])
                                        template_assfeature.attributes['status'] = 3
                                        assignment_flayer.edit_features(updates = [template_assfeature])
                                        print(f"Assignment status for {sResult[0].title} survey is set to Complete!")
                        

    except:
        print(f"There is no {sResult[0].title} feature or field exist!")
        pass

# calling services and webhooks processing functions
getFeatureServices()
webhooksProcessing(av_rows, av_result, av_fset, av_flayer)

# getFeatureServices()
# webhooksProcessing(anc_rows, anc_result, anc_fset, anc_flayer)

# getFeatureServices()
# webhooksProcessing(asb_rows, asb_result, asb_fset, asb_flayer)

# getFeatureServices()
# webhooksProcessing(cfa_rows, cfa_result, cfa_fset, cfa_flayer)

# getFeatureServices()
# webhooksProcessing(cli_rows, cli_result, cli_fset, cli_flayer)

getFeatureServices()
webhooksProcessing(hw_rows, hw_result, hw_fset, hw_flayer)

# getFeatureServices()
# webhooksProcessing(nc_rows, nc_result, nc_fset, nc_flayer)

# getFeatureServices()
# webhooksProcessing(sp_rows, sp_result, sp_fset, sp_flayer)

# getFeatureServices()
# webhooksProcessing(sb_rows, sb_result, sb_fset, sb_flayer)

# getFeatureServices()
# webhooksProcessing(ta_rows, ta_result, ta_fset, ta_flayer)

# getFeatureServices()
# webhooksProcessing(th_rows, th_result, th_fset, th_flayer)

# getFeatureServices()
# webhooksProcessing(tres_rows, tres_result, tres_fset, tres_flayer)

# getFeatureServices()
# webhooksProcessing(van_rows, van_result, van_fset, van_flayer)

# Flagging Vehicle Prestart Check process status to TRUE and its assingment to Complete
getFeatureServices()
try:
    if vpc_fset.features:
        sKeys = []
        sValues = []
        sDict = {}
        sKeys = [av_oid for av_oid in vpc_rows.objectid]
        sValues = [int(av_sid) for av_sid in vpc_rows.customfeatureid]
        sDict = dict(zip(sKeys, sValues))

        for index, vpc_row in vpc_rows.iterrows():
            if vpc_row['process_status'] == 'FALSE' and vpc_result[0].title == 'Vehicle_Prestart_Check':
                print("Updating Vehicle Prestart Check Survey Feature Service ...")
                template_avfeature = deepcopy(vpc_fset.features[index])
                template_avfeature.attributes['process_status'] = 'TRUE'
                vpc_flayer.edit_features(updates = [template_avfeature])
                print(f"{vpc_result[0].title} Processed Status is set to True!")
                for index, row_ass in assignment_rows.iterrows():
                    if int(row_ass.workorderid) in sDict.values() and                         row_ass.description == 'Vehicle Prestart Check' and row_ass.status == 1:

                        #print(row_ass.workorderid, row_cfs.customer_firstname, row_cfs.customer_lastname)
                        template_assfeature = deepcopy(assignment_fset.features[index])
                        template_assfeature.attributes['status'] = 3
                        assignment_flayer.edit_features(updates = [template_assfeature])
                        print(f"Assignment status for {vpc_result[0].title} survey is set to Complete!")
                break
    else:
        print("Vehicle prestart check is not available!")
except:
    pass
#==========================================================================

# Preparing CFS and WF assignments FS to add an assignment row
getFeatureServices()
features_to_be_added = []
# get a template feature object
template_Assignmentfeature_pre = deepcopy(assignment_fset.features[0])
template_cfsfeature = deepcopy(cfs_fset.features[0])

cfs_data = cfs_result[0].layers[0]
assignment_data = assignment_result[0].layers[0]
# Get Unprocessed, Immediate Response and Incomplete process features
cfs_fset = cfs_layers[0].query()
cfs_rows = cfs_fset.sdf

# Handling worker id
getFeatureServices()
keys = [k for k in worker_rows.userid]
values = [v for v in worker_rows.objectid]
worker_dict = dict(zip(keys, values))

# Vehicle prestart check function
def processVPreStart(row, new_feature, cfs_fset, worker_id):
    try:
        success = False
        #get geometries in the destination coordinate system
        input_geometry = {'y':float(cfs_fset.features[index].geometry['y']),
                           'x':float(cfs_fset.features[index].geometry['x'])}
        output_geometry = geometry.project(geometries = [input_geometry],
                                           in_sr = 3857, 
                                           out_sr = cfs_fset.spatial_reference['latestWkid'],
                                           gis = gis)

        # assign the updated values
        new_feature.geometry = output_geometry[0]
        new_feature.attributes['description'] = str("Vehicle Prestart Check")
        new_feature.attributes['status'] = 1
        new_feature.attributes['notes'] = row['CUSTOMER_COMMENT']
        new_feature.attributes['priority'] = int(3)
        new_feature.attributes['assignmenttype'] = int(14)
        if row['OBJECTID']:
            new_feature.attributes['workorderid'] = int(row['OBJECTID'])
        else:
            # arcpy.AddError("Object ID not specified, setting to 0")
            new_feature.attributes['workorderid'] = 0
        new_feature.attributes['duedate'] = today
        if worker_id:
            new_feature.attributes['workerid'] = int(worker_id)
        else:
            # arcpy.AddError("Worker ID not specified, setting to 0")
            new_feature.attributes['workerid'] = 0
#         new_feature.attributes['globalid'] = row['globalid']
#         new_feature.attributes['location'] = row['incident_location']
#         new_feature.attributes['declinedcomment'] = row['declinedcomment']
        new_feature.attributes['assigneddate'] = today
#         new_feature.attributes['assignmentread'] = row['assignmentread']
#         new_feature.attributes['inprogressdate'] = row['inprogressdate']
        new_feature.attributes['completeddate'] = today
#         new_feature.attributes['declineddate'] = row['declineddate']
#         new_feature.attributes['pauseddate'] = row['pauseddate']
#         new_feature.attributes['dispatcherid'] = row['officer']
        new_feature.attributes['created_user'] = row['OFFICER']
        new_feature.attributes['created_date'] = today
        new_feature.attributes['last_edited_user'] = row['OFFICER']
#         new_feature.attributes['last_edited_date'] = row['editdate']
        
        success = True

    except Exception as e:
        print(e)
        print("Error in Adding to Vehicle Prestart check Assignments Feature Service!")

# Handling vehicle prestart check
getFeatureServices()
result = []
for index, row_cfs in cfs_rows.iterrows():
    if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
        today = datetime.datetime.today()
        if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
            #print("within timeframe") 
            if row_cfs['OFFICER'] in worker_dict.keys():
                #print(row['officer'])
                worker_id = worker_dict[row_cfs['OFFICER']]
                from datetime import date
                if len(assignment_rows) > 0:
                    for index, row_ass in assignment_rows.iterrows():
                        creationDate = datetime.datetime.strptime(str(row_ass['created_date']), '%Y-%m-%d %H:%M:%S')
                        if row_ass['workerid'] == worker_id and creationDate.date() == date.today():
                            result.append(worker_id)
                            print("worker ids and dates matched!")
                            break
                    
                else:
                    result.append(worker_id)
            else:
                worker_id = -1 #flag as -1 to prevent further processsing
                        
# print(result)                          
if len(result) == 0:
    print("Adding Vehicle Prestart check to Assignment feature service .....")
    for index, row in cfs_rows.iterrows():
        new_feature = flush_up(deepcopy(template_Assignmentfeature_pre))
        if row['IMMEDIATE_RESPONSE'] == 'No':
            if row['PROCESSED_STATUS'] == 'Incomplete' or row['PROCESSED_STATUS'] == 'Pending':
                today = datetime.datetime.today()
                if row['START_DATE'] <= today <= row['END_DATE']:
                    print("within timeframe")
                    if row['OFFICER'] in worker_dict.keys():
                        worker_id = worker_dict[row['OFFICER']]
                    else:
                        print("Worker is not in WorkForce list!")
                    processVPreStart(row, new_feature, cfs_fset, worker_id)
                    #add this to the list of features to be updated
                    features_to_be_added.append(new_feature)
                    break  

    assignment_layers[0].edit_features(adds = features_to_be_added) 
    print("Assignments feature service is updated with Vehicle Prestart check!")
else:
    print("Vehicle Prestart check is already done!")


def processDispatchRequest(row, new_feature, cfs_fset, worker_id):
    try:
        success = False
        #new_feature = deepcopy(template_Assignmentfeature)
        print("Adding to Assignments Feature Service ...")
        #get geometries in the destination coordinate system
        input_geometry = {'y':float(cfs_fset.features[index].geometry['y']),
                           'x':float(cfs_fset.features[index].geometry['x'])}
        output_geometry = geometry.project(geometries = [input_geometry],
                                           in_sr = 3857, 
                                           out_sr = cfs_fset.spatial_reference['latestWkid'],
                                           gis = gis)

        # assign the updated values
        new_feature.geometry = output_geometry[0]
        new_feature.attributes['description'] = row['DETAILS']
        new_feature.attributes['status'] = 1
        new_feature.attributes['notes'] = row['CUSTOMER_COMMENT']
        if row['PRIORITY'] :
            new_feature.attributes['priority'] = int(row['PRIORITY'])
        else:
            # arcpy.AddError("Priority not specified, setting to 0")
            new_feature.attributes['priority'] = 0
        if row['RESPONSE_SURVEY_TYPE']:
            new_feature.attributes['assignmenttype'] = int(row['RESPONSE_SURVEY_TYPE'])
        else:
            # arcpy.AddError("Response survey type not specified, setting to 0")
            new_feature.attributes['assignmenttype'] = 0
        if row['OBJECTID']:
            new_feature.attributes['workorderid'] = int(row['OBJECTID'])
        else:
            # arcpy.AddError("Object ID not specified, setting to 0")
            new_feature.attributes['workorderid'] = 0
        new_feature.attributes['duedate'] = row['END_DATE']
        if worker_id:
            new_feature.attributes['workerid'] = int(worker_id)
        else:
            # arcpy.AddError("Worker ID not specified, setting to 0")
            new_feature.attributes['workerid'] = 0
#         new_feature.attributes['globalid'] = row['globalid']
        new_feature.attributes['location'] = row['INCIDENT_LOCATION']
#         new_feature.attributes['declinedcomment'] = row['declinedcomment']
        new_feature.attributes['assigneddate'] = row['START_DATE']
#         new_feature.attributes['assignmentread'] = row['assignmentread']
#         new_feature.attributes['inprogressdate'] = row['inprogressdate']
        new_feature.attributes['completeddate'] = row['END_DATE']
#         new_feature.attributes['declineddate'] = row['declineddate']
#         new_feature.attributes['pauseddate'] = row['pauseddate']
#         new_feature.attributes['dispatcherid'] = row['officer']
        new_feature.attributes['created_user'] = row['OFFICER']
        new_feature.attributes['created_date'] = row['START_DATE']
        new_feature.attributes['last_edited_user'] = row['OFFICER']
#         new_feature.attributes['last_edited_date'] = row['editdate']
        
        success = True

    except Exception as e:
        print(e)
        print("Error in Adding to Assignments Feature Service!")
        
# Add a row to WF assignments and update CFS with process status, immediate response status
# and update WF worker ID
getFeatureServices()
template_Assignmentfeature_s = flush_up(deepcopy(assignment_fset.features[0]))
features_to_be_added = []
for index, row in cfs_rows.iterrows():
    new_feature = deepcopy(template_Assignmentfeature_s)
    if row['IMMEDIATE_RESPONSE'] == 'No':
        if row['PROCESSED_STATUS'] == 'Incomplete' or row['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            l_date = datetime.datetime.strptime(str(row['END_DATE']), '%Y-%m-%d %H:%M:%S')
            delta = l_date - today
            if row['START_DATE'] <= today <= row['END_DATE']:
                print("within timeframe")
                if row['OFFICER'] in worker_dict.keys():
                    worker_id = worker_dict[row['OFFICER']]
                else:
                    print("Worker is not in WorkForce list!")

                processDispatchRequest(row, new_feature, cfs_fset, worker_id)
                #add this to the list of features to be updated
                features_to_be_added.append(new_feature)

                cfs_fset_incomplete = cfs_layers[0].query(where="IMMEDIATE_RESPONSE = 'No' and (PROCESSED_STATUS = 'Incomplete' or PROCESSED_STATUS = 'Pending')")
                if (delta.days) >= 0:
                    print("More than one day left!")
                    if cfs_fset_incomplete:
                        cfs_fset_incomplete.features[0].attributes['PROCESSED_STATUS'] = 'Pending'
#                             cfs_fset_incomplete.features[0].attributes['IMMEDIATE_RESPONSE'] = 'No'
                        cfs_data.edit_features(updates = cfs_fset_incomplete.features) 
                        print("Custom feature service is set to Pending!")
                    else:
                        print("There is no Incomplete row to set as Pending!")
            elif today > row['END_DATE']: 
                cfs_fset_incomplete = cfs_layers[0].query(where="IMMEDIATE_RESPONSE = 'No' and (PROCESSED_STATUS = 'Incomplete' or PROCESSED_STATUS = 'Pending')")
                if (delta.days) < 0:
                    print("Not more than one day left!")
                    if cfs_fset_incomplete:
                        cfs_fset_incomplete.features[0].attributes['PROCESSED_STATUS'] = 'Incomplete'
                        cfs_data.edit_features(updates = cfs_fset_incomplete.features) 
                        print("Custom feature service is overdue and is still incomplete!")
                    else:
                        print("There is no Incomplete/Pending row!")
                else:
                    print("Not within timeframe!")

if len(features_to_be_added):
    assignment_layers[0].edit_features(adds = features_to_be_added) 
    print("Assignments feature service is updated with immediate response!")
else:
    print("There is no incomplete or pending job for assignments!")

# Update CFS with assignment ID
getFeatureServices()
assign_workorderids = [k for k in assignment_rows.workorderid]
assign_objectids = [v for v in assignment_rows.objectid]
assign_dict = dict(zip(assign_workorderids, assign_objectids))

for index, row_cfs in cfs_rows.iterrows():
    if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
        today = datetime.datetime.today()
        if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
            if str(row_cfs.OBJECTID) in assign_dict.keys():
                assign_id = assign_dict[str(row_cfs.OBJECTID)]
                template_cfsfeature = deepcopy(cfs_fset.features[index])
                template_cfsfeature.attributes['wf_assignment_ids'] = assign_id
                cfs_data.edit_features(updates = [template_cfsfeature]) 
                print("Custom feature service is updated with workforce assignment ID!")
            else:
                print("CFS ID does not match with assignment ID!")
        else:
            print("No within timeframe")
    else:
        print("There are no Incomplete or Pending jobs!")

# Handling CFS customer request status with number of surveys 
# Get worker id and assignment type from WF assignment feature service
getFeatureServices()
assign_workerid = [workerid for workerid in assignment_rows.workerid]
assign_assignmenttype = [assignmenttype for assignmenttype in assignment_rows.assignmenttype]
assign_dict_wat = dict(zip(assign_workerid,assign_assignmenttype))

NoOfDays = None
NoOfSurveys = []
for index, row_cfs in cfs_rows.iterrows():
    try:
        if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                NoOfDays = (row_cfs['END_DATE'] - row_cfs['START_DATE']).days
                for index, row_ass in assignment_rows.iterrows():
                    if row_cfs.OFFICER  in worker_dict.keys():
                         if row_ass.workerid == worker_id and row_ass.assigneddate.date() == row_cfs['START_DATE'].date()                             and row_ass.duedate.date() == row_cfs['END_DATE'].date():

                                assign_type = assign_dict_wat[row_ass.workerid]
#                                 print(assign_type)
                                NoOfSurveys.append(assign_dict_wat[assign_type])
    except Exception as e:
        print(e)


if (NoOfDays == len(NoOfSurveys)):
    print("Number of days are equal to the number of surveys!")
    print("Setting Customer Request Status in CFS to True ....")
    for index, row_cfs in cfs_rows.iterrows():
        if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                template_cfsfeature = deepcopy(cfs_fset.features[index])
                template_cfsfeature.attributes['CUSTOMER_REQUEST_STATUS'] = "True"
                cfs_data.edit_features(updates = [template_cfsfeature]) 
                print("Set Customer Request Status in CFS to True!")
elif (NoOfDays != len(NoOfSurveys)):
    print("Number of days are not equal to the number of surveys!")
    print("Setting Customer Request Status in CFS to False ....")
    for index, row_cfs in cfs_rows.iterrows():
        if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                template_cfsfeature = deepcopy(cfs_fset.features[index])
                template_cfsfeature.attributes['CUSTOMER_REQUEST_STATUS'] = "False"
                cfs_data.edit_features(updates = [template_cfsfeature]) 
                print("Set Customer Request Status in CFS to False!")
else:
    print("There are no Incomplete or Pending assignments!")


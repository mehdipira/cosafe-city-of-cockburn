#-------------------------------------------------------------------------------
# Name:        Add WorkForce assignments from custom feature service - Immediate responses
# Purpose:      To be published as GP tool to Portal
#
# Author:      mpira
#
# Created:     05/05/2020
# Copyright:   (c) mpira 2020
# Licence:     <your licence>
#-------------------------------------------------------------------------------

# Connect to the GIS
from arcgis.gis import GIS
from arcgis import features
import pandas as pd
# import config_cfs_assign
# from getpass import getpass
from arcgis import geometry
from copy import deepcopy
import datetime
# import arcpy

url = 'https??ab-prd-gis-03.cockburn.wa.gov.au?portal'
url = url.replace('??', '://')
url = url.replace('?', ':/')
# url = 'https://ab-prd-gis-03.cockburn.wa.gov.au/portal'
username = 'adm.esriservices'
password = 'D$Usl\gr'

gis = GIS(url, username, password)

cfs_result, cfs_rows, assignment_result, assignment_rows, worker_result, worker_rows = None, None, None, None, None, None
cfs_layers, cfs_flayer, cfs_fset = None, None, None
assignment_layers, assignment_flayer, assignment_fset = None, None, None
worker_layers, worker_flayer, worker_fset = None, None, None
new_feature = None
worker_id = None
process_count = 0
wf_count = 0
features_to_be_added = []



def flush_up(temp):
    temp.attributes['description'] = None
    temp.attributes['status'] = None
    temp.attributes['notes'] = None
#     temp.attributes['priority'] = -1
    temp.attributes['assignmenttype'] = -1
    temp.attributes['workorderid'] = -1
    temp.attributes['duedate'] = None

    return temp




def getFeatureServices():
    # search for the custom feature service
    global cfs_result
    global cfs_layers
    global cfs_flayer
    global cfs_fset
    global cfs_rows
    global process_count
    global wf_count
    cfs_result = gis.content.search('type: "Feature Service" AND title:"Dispatch_CoSafe_CFS"')

    # access the item's feature layers
    cfs_layers = cfs_result[0].layers
    cfs_flayer = cfs_layers[0]
    # query all the features and display table
    cfs_fset = cfs_layers[0].query()
    if len(cfs_fset.features) > 0:
        cfs_rows = cfs_fset.sdf
    else:
        print("There is no features. First create a feature in Dispatch CoSafe CFS!")

    # search for the assignments feature service
    global assignment_result
    global assignment_layers
    global assignment_flayer
    global assignment_fset
    global assignment_rows
    assignment_result = gis.content.search(
        'type: "Feature Service" AND title:"assignments_836f26b1ba584eeca72b517ac71715a7"')
    # access the item's feature layers
    assignment_layers = assignment_result[0].layers
    assignment_flayer = assignment_layers[0]
    # query all the features and display its table
    assignment_fset = assignment_layers[0].query()
    if len(assignment_fset.features) > 0:
        assignment_rows = assignment_fset.sdf
    else:
        print("There is no features. First create a feature in assignment!")

    # search for the Workers feature service
    global worker_result
    global worker_layers
    global worker_flayer
    global worker_fset
    global worker_rows
    worker_result = gis.content.search('type: "Feature Service" AND title:"workers_836f26b1ba584eeca72b517ac71715a7"')
    # access the item's feature layers
    worker_layers = worker_result[0].layers
    worker_flayer = worker_layers[0]
    # query all the features and display table
    worker_fset = worker_layers[0].query()
    if len(worker_fset.features) > 0:
        worker_rows = worker_fset.sdf
    else:
        print("There is no features. First create a feature in assignment!")


# Preparing CFS and WF assignments FS to add an assignment row
getFeatureServices()
features_to_be_added = []
# get a template feature object
template_Assignmentfeature_pre = deepcopy(assignment_fset.features[0])
template_cfsfeature = deepcopy(cfs_fset.features[0])

cfs_data = cfs_result[0].layers[0]
assignment_data = assignment_result[0].layers[0]
# Get Unprocessed, OnDemand, Immediate Response and Incomplete process features
cfs_fset = cfs_layers[0].query()
cfs_rows = cfs_fset.sdf

# Handling worker id
getFeatureServices()
keys = [k for k in worker_rows.userid]
values = [v for v in worker_rows.objectid]
worker_dict = dict(zip(keys, values))


# Vehicle prestart check function
def processVPreStart(row, new_feature, cfs_fset, worker_id):
    try:
        success = False
        # get geometries in the destination coordinate system
        input_geometry = {'y': float(cfs_fset.features[index].geometry['y']),
                          'x': float(cfs_fset.features[index].geometry['x'])}
        output_geometry = geometry.project(geometries=[input_geometry],
                                           in_sr=3857,
                                           out_sr=cfs_fset.spatial_reference['latestWkid'],
                                           gis=gis)

        # assign the updated values
        new_feature.geometry = output_geometry[0]
        new_feature.attributes['description'] = str("Vehicle Prestart Check")
        new_feature.attributes['status'] = 1
        new_feature.attributes['notes'] = row['CUSTOMER_COMMENT']
        new_feature.attributes['priority'] = int(3)
        new_feature.attributes['assignmenttype'] = int(14)
        if row['OBJECTID']:
            new_feature.attributes['workorderid'] = int(row['OBJECTID'])
        else:
            # arcpy.AddError("Object ID not specified, setting to 0")
            new_feature.attributes['workorderid'] = 0
        new_feature.attributes['duedate'] = today
        if worker_id:
            new_feature.attributes['workerid'] = int(worker_id)
        else:
            # arcpy.AddError("Worker ID not specified, setting to 0")
            new_feature.attributes['workerid'] = 0
        #         new_feature.attributes['globalid'] = row['globalid']
        #         new_feature.attributes['location'] = row['incident_location']
        #         new_feature.attributes['declinedcomment'] = row['declinedcomment']
        new_feature.attributes['assigneddate'] = today
        #         new_feature.attributes['assignmentread'] = row['assignmentread']
        #         new_feature.attributes['inprogressdate'] = row['inprogressdate']
        new_feature.attributes['completeddate'] = today
        #         new_feature.attributes['declineddate'] = row['declineddate']
        #         new_feature.attributes['pauseddate'] = row['pauseddate']
        #         new_feature.attributes['dispatcherid'] = row['officer']
        new_feature.attributes['created_user'] = row['OFFICER']
        new_feature.attributes['created_date'] = today
        new_feature.attributes['last_edited_user'] = row['OFFICER']
        #         new_feature.attributes['last_edited_date'] = row['editdate']

        success = True

    except Exception as e:
        print(e)
        print("Error in Adding to Vehicle Prestart check Assignments Feature Service!")


# Handling vehicle prestart check
getFeatureServices()
result = []
for index, row_cfs in cfs_rows.iterrows():
    if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
        today = datetime.datetime.today()
        if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
            # print("within timeframe")
            if row_cfs['OFFICER'] in worker_dict.keys():
                # print(row['officer'])
                worker_id = worker_dict[row_cfs['OFFICER']]
                from datetime import date

                if len(assignment_rows) > 0:
                    for index, row_ass in assignment_rows.iterrows():
                        creationDate = datetime.datetime.strptime(str(row_ass['created_date']), '%Y-%m-%d %H:%M:%S')
                        if row_ass['workerid'] == worker_id and creationDate.date() == date.today():
                            result.append(worker_id)
                            print("worker ids and dates matched!")
                            break

                else:
                    result.append(worker_id)
            else:
                worker_id = -1 #flag as -1 to prevent further processsing

print(result)
if len(result) == 0:
    print("Adding Vehicle Prestart check to Assignment feature service .....")
    for index, row in cfs_rows.iterrows():
        new_feature = flush_up(deepcopy(template_Assignmentfeature_pre))
        #         new_feature = deepcopy(assignment_fset.features[0])

        if row['IMMEDIATE_RESPONSE'] == 'Yes':
            if row['PROCESSED_STATUS'] == 'Incomplete':
                today = datetime.datetime.today()
                if row['START_DATE'] <= today <= row['END_DATE']:
                    print("within timeframe")
                    if row['OFFICER'] in worker_dict.keys():
                        worker_id = worker_dict[row['OFFICER']]
                    else:
                        print("Worker is not in Workforce list!")
                    processVPreStart(row, new_feature, cfs_fset, worker_id)
                    # add this to the list of features to be updated
                    features_to_be_added.append(new_feature)
                    break

    assignment_layers[0].edit_features(adds=features_to_be_added)
    wf_count = wf_count + 1
    print("Assignments feature service is updated with Vehicle Prestart check!")
#     print(len(features_to_be_added))
else:
    print("Vehicle Prestart check is already done!")


def processDispatchRequest(row, new_feature, cfs_fset, worker_id):
    try:
        success = False
        # new_feature = deepcopy(template_Assignmentfeature)
        print("Adding to Assignments Feature Service ...")
        # get geometries in the destination coordinate system
        input_geometry = {'y': float(cfs_fset.features[index].geometry['y']),
                          'x': float(cfs_fset.features[index].geometry['x'])}
        output_geometry = geometry.project(geometries=[input_geometry],
                                           in_sr=3857,
                                           out_sr=cfs_fset.spatial_reference['latestWkid'],
                                           gis=gis)

        # assign the updated values
        new_feature.geometry = output_geometry[0]
        new_feature.attributes['description'] = row['DETAILS']
        new_feature.attributes['status'] = 1
        new_feature.attributes['notes'] = row['CUSTOMER_COMMENT']
        if row['PRIORITY'] :
            new_feature.attributes['priority'] = int(row['PRIORITY'])
        else:
            # arcpy.AddError("Priority not specified, setting to 0")
            new_feature.attributes['priority'] = 0
        if row['RESPONSE_SURVEY_TYPE']:
            new_feature.attributes['assignmenttype'] = int(row['RESPONSE_SURVEY_TYPE'])
        else:
            # arcpy.AddError("Response survey type not specified, setting to 0")
            new_feature.attributes['assignmenttype'] = 0
        if row['OBJECTID']:
            new_feature.attributes['workorderid'] = int(row['OBJECTID'])
        else:
            # arcpy.AddError("Object ID not specified, setting to 0")
            new_feature.attributes['workorderid'] = 0
        new_feature.attributes['duedate'] = row['END_DATE']
        if worker_id:
            new_feature.attributes['workerid'] = int(worker_id)
        else:
            # arcpy.AddError("Worker ID not specified, setting to 0")
            new_feature.attributes['workerid'] = 0
        #         new_feature.attributes['globalid'] = row['globalid']
        new_feature.attributes['location'] = row['INCIDENT_LOCATION']
        #         new_feature.attributes['declinedcomment'] = row['declinedcomment']
        new_feature.attributes['assigneddate'] = row['START_DATE']
        #         new_feature.attributes['assignmentread'] = row['assignmentread']
        #         new_feature.attributes['inprogressdate'] = row['inprogressdate']
        new_feature.attributes['completeddate'] = row['END_DATE']
        #         new_feature.attributes['declineddate'] = row['declineddate']
        #         new_feature.attributes['pauseddate'] = row['pauseddate']
        #         new_feature.attributes['dispatcherid'] = row['officer']
        new_feature.attributes['created_user'] = row['OFFICER']
        new_feature.attributes['created_date'] = row['START_DATE']
        new_feature.attributes['last_edited_user'] = row['OFFICER']
        #         new_feature.attributes['last_edited_date'] = row['editdate']

        success = True

    except Exception as e:
        print(e)
        print("Error in Adding to Assignments Feature Service!")


# Add a row to WF assignments and update CFS with process status, immediate response and on-demand status
# and update WF worker ID
getFeatureServices()
template_Assignmentfeature_s = flush_up(deepcopy(assignment_fset.features[0]))
features_to_be_added = []
for index, row in cfs_rows.iterrows():
    new_feature = deepcopy(template_Assignmentfeature_s)
    if row['IMMEDIATE_RESPONSE'] == 'Yes':
        if row['PROCESSED_STATUS'] == 'Incomplete':
            today = datetime.datetime.today()
            l_date = datetime.datetime.strptime(str(row['END_DATE']), '%Y-%m-%d %H:%M:%S')
            delta = l_date - today
            if row['START_DATE'] <= today <= row['END_DATE']:
                print("within timeframe")
                if row['OFFICER'] in worker_dict.keys():
                    worker_id = worker_dict[row['OFFICER']]
                else:
                    print("Worker is not in WorkForce list!")

                processDispatchRequest(row, new_feature, cfs_fset, worker_id)
                # add this to the list of features to be updated
                features_to_be_added.append(new_feature)

                cfs_fset_incomplete = cfs_layers[0].query(
                    where="IMMEDIATE_RESPONSE = 'Yes' and PROCESSED_STATUS = 'Incomplete'")
                if (delta.days) >= 0:
                    print("More than one day left!")
                    if cfs_fset_incomplete:
                        cfs_fset_incomplete.features[0].attributes['PROCESSED_STATUS'] = 'Pending'
#                             cfs_fset_incomplete.features[0].attributes['ON_DEMAND'] = 'No'
                        cfs_fset_incomplete.features[0].attributes['IMMEDIATE_RESPONSE'] = 'No'
                        cfs_data.edit_features(updates=cfs_fset_incomplete.features)
                        print("Custom feature service is set to Pending!")
                    else:
                        print("There is no Incomplete row to set as Pending!")
            elif today > row['END_DATE']:
                cfs_fset_incomplete = cfs_layers[0].query(
                    where="IMMEDIATE_RESPONSE = 'Yes' and PROCESSED_STATUS = 'Pending'")
                if (delta.days) < 0:
                    print("Not more than one day left!")
                    if cfs_fset_incomplete:
                        cfs_fset_incomplete.features[0].attributes['PROCESSED_STATUS'] = 'Incomplete'
                        cfs_data.edit_features(updates=cfs_fset_incomplete.features)
                        print("Custom feature service is overdue and is still incomplete!")
                    else:
                        print("There is no Incomplete/Pending row!")
                else:
                    print("Not within timeframe!")

if len(features_to_be_added):
    assignment_layers[0].edit_features(adds=features_to_be_added)
    process_count = process_count + 1
    wf_count = wf_count + 1
    print("Assignments feature service is updated with immediate response!")
else:
    print("There is no incomplete or pending job for assignments!")

# Update CFS with assignment ID
getFeatureServices()
assign_workorderids = [k for k in assignment_rows.workorderid]
assign_objectids = [v for v in assignment_rows.objectid]
assign_dict = dict(zip(assign_workorderids, assign_objectids))

for index, row_cfs in cfs_rows.iterrows():
    if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
        today = datetime.datetime.today()
        if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
            if str(row_cfs.OBJECTID) in assign_dict.keys():
                assign_id = assign_dict[str(row_cfs.OBJECTID)]
                template_cfsfeature = deepcopy(cfs_fset.features[index])
                template_cfsfeature.attributes['WF_ASSIGNMENT_IDS'] = assign_id
                cfs_data.edit_features(updates=[template_cfsfeature])
                print("Custom feature service is updated with workforce assignment ID!")
            else:
                print("CFS ID does not match with assignment ID!")
        else:
            print("No within timeframe")
    else:
        print("There are no Incomplete or Pending jobs!")

# Handling CFS customer request status with number of surveys
# Get worker id and assignment type from WF assignment feature service
getFeatureServices()
assign_workerid = [workerid for workerid in assignment_rows.workerid]
assign_assignmenttype = [assignmenttype for assignmenttype in assignment_rows.assignmenttype]
assign_dict_wat = dict(zip(assign_workerid, assign_assignmenttype))

NoOfDays = None
NoOfSurveys = []
for index, row_cfs in cfs_rows.iterrows():
    try:
        if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                NoOfDays = (row_cfs['END_DATE'] - row_cfs['START_DATE']).days
                for index, row_ass in assignment_rows.iterrows():
                    if row_cfs.OFFICER in worker_dict.keys():
                        if row_ass.workerid == worker_id and row_ass.assigneddate.date() == row_cfs[
                            'START_DATE'].date() and row_ass.duedate.date() == row_cfs['END_DATE'].date():
                            assign_type = assign_dict_wat[row_ass.workerid]
                            #                                 print(assign_type)
                            NoOfSurveys.append(assign_dict_wat[assign_type])
    except Exception as e:
        print(e)

if (NoOfDays == len(NoOfSurveys)):
    print("Number of days are equal to the number of surveys!")
    print("Setting Customer Request Status in CFS to True ....")
    for index, row_cfs in cfs_rows.iterrows():
        if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                template_cfsfeature = flush_up(deepcopy(cfs_fset.features[index]))
                template_cfsfeature.attributes['CUSTOMER_REQUEST_STATUS'] = "True"
                cfs_data.edit_features(updates=[template_cfsfeature])
                print("Set Customer Request Status in CFS to True!")
elif (NoOfDays != len(NoOfSurveys)):
    print("Number of days are not equal to the number of surveys!")
    print("Setting Customer Request Status in CFS to False ....")
    for index, row_cfs in cfs_rows.iterrows():
        if row_cfs['PROCESSED_STATUS'] == 'Incomplete' or row_cfs['PROCESSED_STATUS'] == 'Pending':
            today = datetime.datetime.today()
            if row_cfs['START_DATE'] <= today <= row_cfs['END_DATE']:
                template_cfsfeature = flush_up(deepcopy(cfs_fset.features[index]))
                template_cfsfeature.attributes['CUSTOMER_REQUEST_STATUS'] = "False"
                cfs_data.edit_features(updates=[template_cfsfeature])
                print("Set Customer Request Status in CFS to False!")
else:
    print("There are no Incomplete or Pending assignments!")

# arcpy.AddMessage(f"Total number of customer request processed: {process_count}")
# arcpy.AddMessage(f"Total number of workforece assignment scheduled: {wf_count}")






